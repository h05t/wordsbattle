﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChatScript : MonoBehaviour {

	public GUIStyle my_gui_style;

	// ALL words
	public List<string> chatHistory = new List<string>();
	// 
	public List<string> rudeWordsList = new List<string>();
	
	private string currentMsg = string.Empty;
	int i = 0;

	private const uint TextFieldWidth = 200;
	private const uint TextFieldHeight = 30;
	
	Rect textfieldRect = new Rect(Screen.width/2 - TextFieldWidth/2, Screen.height * 5/6 - TextFieldHeight/2, TextFieldWidth, TextFieldHeight);

	void Start()
	{
		rudeWordsList.Add ("1");
		rudeWordsList.Add ("2");
		rudeWordsList.Add ("3");
	}

	private void SendMessage()
	{
		if (!string.IsNullOrEmpty(currentMsg.Trim()))
		{
			networkView.RPC("ChatMessage", RPCMode.Others, new object[] {currentMsg});
			currentMsg = string.Empty;
		}
	}

	private void Bottom()
	{
		Event e = Event.current;

		currentMsg = GUI.TextField (textfieldRect/*new Rect(0, Screen.height - 20, 175, 20)*/, currentMsg);
		//if (GUI.Button (new Rect (200, Screen.height - 20, 75, 20), "Send")) 

		if (e.keyCode == KeyCode.Return) 
		{
			SendMessage ();
		}
			

		GUILayout.Space (15);

		for (int i = chatHistory.Count - 1; i >= 0; i--)
			GUI.Label(textfieldRect, chatHistory[i], my_gui_style);
	}
	private void Top()
	{

		GUILayout.Space (15);
		GUILayout.BeginHorizontal(GUILayout.Width(250));
		currentMsg = GUILayout.TextField(currentMsg);
		//if (GUILayout.Button ("Send"))
		//	SendMessage ();

		Event e = Event.current;
		if (e.keyCode == KeyCode.Return) 
		{
			SendMessage ();
		}
		GUILayout.EndHorizontal ();
		
		foreach (string c in chatHistory)
			GUILayout.Label (c);
	}

	private void OnGUI()
	{
		if (!MultiplayerMenu.Connected)
			return;

		Bottom ();

	}

	[RPC]
	public void ChatMessage(string msg)
	{
		chatHistory.Add (msg);


		string currentRudeWord = "1";//rudeWordsList [i];
		// CHECK
		// 1. word corretness ???

		if (currentRudeWord  == msg) 
		{
			GameParameters.GetInstance().health -= 10;
			GUI.Label(new Rect(100, 100, 20, 20), "EQUAL", my_gui_style);
			// shoiw next word
			i++;

			if (i == rudeWordsList.Count)
					i = 0;
		}
		else
			GUI.Label(new Rect(100, 100, 20, 20), "NOT EQUAL", my_gui_style);
	}
}
