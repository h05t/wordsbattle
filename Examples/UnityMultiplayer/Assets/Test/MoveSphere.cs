﻿using UnityEngine;
using System.Collections;

[RequireComponent( typeof( Rigidbody ) )] // в данном примере нам понадобиться риджедбоди

public class MoveSphere : MonoBehaviour {

	public float power = 20f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
			/*if (Input.GetKey (KeyCode.UpArrow))
						transform.position += Vector3.up * Time.deltaTime * 5;
			if (Input.GetKey(KeyCode.DownArrow))
					transform.position -= Vector3.up * Time.deltaTime * 5;
			*/
		float inputX = Input.GetAxis( "Horizontal" );
		float inputY = Input.GetAxis( "Vertical" );
		if ( inputX != 0.0f ) {
			rigidbody.AddTorque( Vector3.forward * -inputX * power, ForceMode.Impulse );
		}
		if ( inputY != 0.0f ) {
			rigidbody.AddTorque( Vector3.right * inputY * power, ForceMode.Impulse );
		}
	}
}
