﻿using UnityEngine;
using System.Collections;

public class PersonSelectionMenu : MonoBehaviour {
	private const uint GridWidth = 300;
	private const uint GridHeight = 300;

	public int selGridInt = -1; // Сделать статической глобальной, чтоб передавать м/у сценами!!!!!!!!!!!
	public string[] selStrings = new string[] {"Grid 1", "Grid 2", "Grid 3", "Grid 4"};
	void OnGUI() {
		selGridInt = GUI.SelectionGrid(new Rect(Screen.width/2 - GridWidth/2, Screen.height/2 - GridWidth/2, GridWidth, GridHeight), -1, selStrings, 2);
		if(0 <= selGridInt)
		{
			Application.LoadLevel("Multiplayer");
		}
	}
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}