﻿using UnityEngine;
using System.Collections;

public class O2BarManager : MonoBehaviour
{
    public float timeBetweenLostO2 = 1f;
    public int lostO2Amound = 5;
    public int lostHealthWithoutO2Amound = 30;
    float timer; 

    // Update is called once per frame
    void FixedUpdate()
    {

        // Если персонаж жив, то он дишит и может умиреть:
        if (false == GameParameters.GetInstance().isDead)
        {
            // Add the time since Update was last called to the timer.
            timer += Time.deltaTime;

            // If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
            if (timer >= timeBetweenLostO2 && GameParameters.GetInstance().O2 > 0)
            {
                // Сначала изменяем значение в глобальных данных:
                GameParameters.GetInstance().O2 = GameParameters.GetInstance().O2 - lostO2Amound;
                timer = 0f;
            }

            // Если у игрока не осталось кислорода то он начинает умирать:
            if (GameParameters.GetInstance().O2 <= 0 && GameParameters.GetInstance().health > 0 && timer >= timeBetweenLostO2)
            {
                // Сначала изменяем значение в глобальных данных:
                GameParameters.GetInstance().health = GameParameters.GetInstance().health - lostHealthWithoutO2Amound;
                timer = 0f;
            }
        }
    }
}
