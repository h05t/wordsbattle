﻿using UnityEngine;
using System.Collections;

public class BarUpdate : MonoBehaviour
{
    //GameObject O2BarGO;
    //IndicatorBar O2Bar;

    GameObject healthBarGO;
    IndicatorBar healthBar;

    //GameObject fuelBarGO;
    //IndicatorBar fuelBar;

    void Awake()
    {
        GameParameters.GetInstance().isDead = false;

        //O2BarGO = GameObject.FindGameObjectWithTag("O2Bar");
        //O2Bar = O2BarGO.GetComponent<IndicatorBar>();

        healthBarGO = GameObject.FindGameObjectWithTag("healthBar");
        healthBar = healthBarGO.GetComponent<IndicatorBar>();

        //fuelBarGO = GameObject.FindGameObjectWithTag("fuelBar");
        //fuelBar = fuelBarGO.GetComponent<IndicatorBar>();
    }

    // Update is called once per frame
    void Update()
    {
        //O2Bar.O2Width = GameParameters.GetInstance().O2;
        healthBar.O2Width = GameParameters.GetInstance().health;
        //fuelBar.O2Width = GameParameters.GetInstance().fuel;

        // Если у игрока не осталось хелсов то он умер:
        if (GameParameters.GetInstance().health <= 0 && false == GameParameters.GetInstance().isDead)
        {
            // Сначала изменяем значение в глобальных данных:
            GameParameters.GetInstance().isDead = true;

            //transform.gameObject.AddComponent<GameOverScript>();
        }
    }
}
