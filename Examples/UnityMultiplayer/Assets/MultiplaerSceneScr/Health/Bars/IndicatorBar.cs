﻿using UnityEngine;
using System.Collections;

public class IndicatorBar : MonoBehaviour
{
    public Texture backgroundTexture;
    public Texture  foregroundTexture;
    public Texture frameTexture;

    public int O2MarginLeft = 70;
    public int O2MarginTop = 35;

    public int frameMarginLeft = 10;
    public int frameMarginTop = 10;

    public int O2Width = 199;
    int O2Height = 30;

    int frameWidth = 266;
    int frameHeight = 65;

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(frameMarginLeft, frameMarginTop, /*frameMarginLeft +*/ frameWidth, /*frameMarginTop +*/ frameHeight), backgroundTexture, ScaleMode.ScaleToFit, true, 0);
        GUI.DrawTexture(new Rect(O2MarginLeft, O2MarginTop, O2Width/* + O2MarginLeft*/, O2Height), foregroundTexture, ScaleMode.ScaleAndCrop, true, 0);
        GUI.DrawTexture(new Rect(frameMarginLeft, frameMarginTop, frameWidth, frameHeight), frameTexture, ScaleMode.ScaleToFit, true, 0);
    }
}
