﻿using UnityEngine;
using System.Collections;

public class GameParameters
{
    private static GameParameters gameParameters = new GameParameters();
    private GameParameters()
    {
    }

    public static GameParameters GetInstance()
    {
        return gameParameters;
    }

    public void SetStartParam()
    {
        fuel = 199;
        O2 = 199;
        health = 199;
        //isDead = false;    
    }

    // Переделать в проценты!!!!!!!!!!!!!!!!!!!
    public int fuel = 199;
    public int O2 = 199;
    public int health = 199;
    public bool isDead = false;

    // Количество оставшихся в живых монстров-разнорабочих:
    public int enemyCount = 4;
    public bool wakeUpHitler = false;

    // Этапы сценария:
    public bool firstBrainEated = false;
    public bool sawHitler = false;
    public bool hitlerDied = false;
}