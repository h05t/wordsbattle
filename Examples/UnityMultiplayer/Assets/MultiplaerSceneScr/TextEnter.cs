﻿using UnityEngine;
using System.Collections;

public class TextEnter : MonoBehaviour {
	public GUIStyle my_gui_style;	

	public string stringToEdit = "";
	public string stringToHit = "";

	private const uint TextFieldWidth = 200;
	private const uint TextFieldHeight = 30;

	Rect textfieldRect = new Rect(Screen.width/2 - TextFieldWidth/2, Screen.height * 5/6 - TextFieldHeight/2, TextFieldWidth, TextFieldHeight);
	Rect labelRect = new Rect(Screen.width/2 - TextFieldWidth - 30, Screen.height * 5/6 - TextFieldHeight/2, TextFieldWidth, TextFieldHeight);

	void OnGUI() {
		Event e = Event.current;

		GUI.Label(labelRect,"Enter your rude word:", my_gui_style);
		stringToEdit = GUI.TextField(textfieldRect, stringToEdit, 25);

		if (e.keyCode == KeyCode.Return) 
		{
			stringToHit = string.Copy(stringToEdit);
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
